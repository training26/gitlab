# Gitlab

Gitlab in Pricefx
[https://gitlab.pricefx.eu/](https://gitlab.pricefx.eu/)

#HSLIDE

## Gitlab

Ambition to replace Atlassian stack

#HSLIDE

## Gitlab in Pricefx

- Building Docker images
- Container build pipeline
- Pipeline triggers
- Docker registry

#HSLIDE

## Image build pipeline

- On commit - build container with image tag
    - commit hash
    - latest
- On git tag - build container with image tag
    - git tag
    - latest
[IM build pipeline](https://gitlab.pricefx.eu/docker-images/integration-manager/blob/master/.gitlab-ci.yml)

#HSLIDE

## Pipeline triggers

- [Manual](https://gitlab.pricefx.eu/docker-images/integration-manager)
- On push
- [HTTP trigger](https://gitlab.pricefx.eu/docker-images/integration-manager/-/settings/ci_cd)
    - Variable passing (e.g. Maven Version) 

#HSLIDE

## Docker registry

- Central storage of docker images (like Artifactory/Nexus for mvn artifacts)
- Push/Pull
- [IM images](https://gitlab.pricefx.eu/docker-images/integration-manager/container_registry)

#HSLIDE
## Q&A

